package com.xnx3.wangmarket.plugin.learnExample.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.BaseVO;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;

/**
 * 功能插件开发入门示例
 * @author 管雷鸣
 */
@Controller(value="LearnExampleIndexPluginController")
@RequestMapping("/plugin/learnExample/")
public class IndexController extends BasePluginController {
	
	/**
	 * 演示json请求 demo
	 */
	@ResponseBody
	@RequestMapping("demo.json")
	public BaseVO demo(){
		/*
		 * 这里可进行逻辑控制等
		 */
		
		//最后返回json格式数据
		return success("操作成功");
	}
}